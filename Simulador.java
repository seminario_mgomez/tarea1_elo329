import java.io.PrintStream;
import java.util.*;
import java.lang.Math;
public class Simulador {
    private Comuna comuna;
    private PrintStream out;
    private ArrayList<Individuo> poblacion;
    private double speed;
    private double deltaAngle;
    private double mascarillas;
    private double infectadoLimitTime;
    private int individuos;
    private int infectados;
    private int numVac;
    private double vacSize;
    private double vacTime;
    private double recuperadoLimitTime;
    private ArrayList<Vacunatorio> vacunatorios;
    private Simulador(){ }

    public Simulador (PrintStream output, Comuna comuna, double speed, double deltaAngle, double infectadoLimitTime, int individuos, int infectados, double mascarillas, int numVac, double vacSize, double vacTime,double recuperadoLimitTime){
        poblacion = null;
        out=output;
        this.comuna = comuna;
        this.deltaAngle = deltaAngle;
        this.speed = speed;
        this.infectadoLimitTime = infectadoLimitTime;
        this.individuos = individuos;
        this.infectados = infectados;
        this.mascarillas = mascarillas;
        this.numVac = numVac;
        this.vacSize = vacSize;
        this. vacTime = vacTime;
        this.vacunatorios = null;
        this.recuperadoLimitTime = recuperadoLimitTime;
    }
    private void poblacionBuilder(int N, int I) {
        poblacion = new ArrayList<Individuo>();
        double peopleWithMask = Math.floor(mascarillas * N);
        for (int i = 0; i < N; i++) {
            if (i < I && i < peopleWithMask) {
                poblacion.add(new Individuo(this.comuna, this.speed, this.deltaAngle, 'i', infectadoLimitTime, true, recuperadoLimitTime));
            } if (i < I && i >= peopleWithMask) {
                poblacion.add(new Individuo(this.comuna, this.speed, this.deltaAngle, 'i', infectadoLimitTime, false, recuperadoLimitTime));
            } if (i >= I && i < peopleWithMask) {
                poblacion.add(new Individuo(this.comuna, this.speed, this.deltaAngle, 's', infectadoLimitTime, true, recuperadoLimitTime));
            } if (i >= I && i >= peopleWithMask) {
                poblacion.add(new Individuo(this.comuna, this.speed, this.deltaAngle, 's', infectadoLimitTime, false, recuperadoLimitTime));
            }
            this.comuna.setPoblacion(poblacion);
        }
    }//crea el arraList de individuos y se los asigna a la comuna
    private void buildVacunatorios(int N){
        vacunatorios = new ArrayList<Vacunatorio>();
        for(int i=0; i<N; i++){
            vacunatorios.add(new Vacunatorio(vacSize,vacTime,comuna));
        }
        comuna.setVacunatorios(vacunatorios);
    } //crea el arrayList de vacunatorios y se los asigna a la comuna
    private void printStateDescription(){;
        String s="time;"+Comuna.getStateDescription();
        out.println(s);
    }//muestra por pantalla la descripcion de los parametros
    private void printState(double t){
        String s = t + ";";
        s+= comuna.getState();
        out.println(s);
    }//muestra por pantalla los parametros
    /**
     * @param delta_t time step
     * @param endTime simulation time
     * @param samplingTime  time between printing states to not use delta_t that would generate too many lines.
     */
    public void simulate (double delta_t, double endTime, double samplingTime) {  // simulate time passing
        poblacionBuilder(individuos, infectados);
        buildVacunatorios(numVac);
        double t=0;
        printStateDescription();
        printState(t);
        while (t<endTime) {
            for(double nextStop=t+samplingTime; t<nextStop; t+=delta_t) {
                comuna.computeNextState(delta_t); // compute its next state based on current global state
                comuna.updateState();            // update its state
            }
            printState(t);
        }
    }//crea la simulacion
}

# Tarea 1 
Para la entrega de esta tarea (y en pro de facilitar la evaluación de esta) creamos diferentes branches para cada stage, de forma que al ultimo stage (Stage_extra) se le hizo un merge al master.

> *Como grupo si decidimos optar por la puntuacion extra.* 

### Archivos del programa

* Individuo.java
* Comuna.java
* Simulador.java
* Stage5.java
* Archivo_de_entrada.txt
* salida.csv
* Vacunatorio.java 

### Requisitos :
* contar con una versión de SDK 11 o superior (ya que esto garantiza el funcionamiento del codigo).

### Instrucciones de compilación 
* Para correr el programa debemos ejecutar el siguiente comando en la terminal : 

```bash
$ make 
```
```bash
$ make run
```

> *Lo anterior es equivalente a ejecutar.*   
```bash
$ javac Stage5.java Individuo.java Comuna.java Simulador.java Vacunatorio.java
``` 
```bash
$ java Stage5 Archivo_de_entrada.txt > salida.csv
```
### Integrantes del equipo:
* Diego Almonacid
* Gabriela Bustamante 
* Ignacio Barrera
* Mariapaz Gómez


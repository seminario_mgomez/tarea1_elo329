public class Individuo {
    private double x, y, speed, angle, deltaAngle;
    private double x_tPlusDelta, y_tPlusDelta;
    private Comuna comuna;
    private char estado;//estado actual
    private char estado_tPlusDelta;//estado en t + deltat
    private double infectadoTime;//contador tiempo de infectado. infectadoTime=-1 indica que aun no se infecta o esta recuperado
    private double infectadoLimitTime;//tiempo de infeccion limite
    private boolean mascarilla;
    private double recuperadoTime;
    private double recuperadoLimitTime;
    public Individuo (Comuna comuna, double speed, double deltaAngle, char estado, double infectadoLimitTime, boolean mascarilla, double recuperadoLimitTime){
        angle = Math.random()*2*Math.PI;
        double width = comuna.getWidth();
        double largo = comuna.getHeight();
        this.x=Math.random()*width;
        this.y=Math.random()*largo;
        this.comuna = comuna;
        this.speed = speed;
        this.deltaAngle = deltaAngle;
        this.x_tPlusDelta = this.x;
        this.y_tPlusDelta = this.y;
        this.estado = estado;
        this.estado_tPlusDelta = estado;
        if(this.infectado()){
            initInfectadoTimer();
        }else{
            infectadoTime = -1;
        }
        this.infectadoLimitTime = infectadoLimitTime;
        this.recuperadoLimitTime = recuperadoLimitTime;   
        this.recuperadoTime = -1;
        this.mascarilla = mascarilla;
    }
    public double getX(){return this.x;}
    public double getY(){return this.y;}
    public double distanceTo(Individuo individuo){
        double xf = individuo.getX();
        double yf = individuo.getY();
        double dx = (xf - this.x);
        double dy = (yf-this.y);
        double distance = Math.sqrt(dx*dx + dy*dy);
        return distance;
    }//entrega la distancia entre el individuo actual y el pasado como parametro
    public boolean contactoEstrecho(Individuo individuo,double d){
        if (distanceTo(individuo)<=d){
            return true;
        }else{
            return false;
        }
    }//devuelve true en caso de que la distancia entre individuos sea menor a la maxima necesaria para establecer contagio
    public boolean infectado(){
        return this.estado=='i';
    }
    public boolean suceptible(){
        return this.estado=='s';
    }
    public boolean recuperado(){
        return this.estado=='r';
    }
    public boolean vacunado(){
        return this.estado=='v';
    }
    public static void Exposicion(Individuo individuo1, Individuo individuo2, double probabilidad_0,double probabilidad_1,double probabilidad_2){
        if(individuo1.mascarilla && individuo2.mascarilla){
            makeExposicion(individuo1, individuo2, probabilidad_2);
        }
        if((!individuo1.mascarilla && individuo2.mascarilla) || (individuo1.mascarilla && !individuo2.mascarilla)){
            makeExposicion(individuo1, individuo2, probabilidad_1);
        }
        if(!individuo1.mascarilla && !individuo2.mascarilla){
            makeExposicion(individuo1, individuo2, probabilidad_0);
        }

    }//se encarga de simular la exposición entre dos individuos en contacto estrecho considerando los tres casos
    public static void makeExposicion(Individuo individuo1, Individuo individuo2, double P){
        if(Math.random() < P){
            if(individuo1.infectado() && individuo2.suceptible()){
                individuo2.estado_tPlusDelta = 'i';
            }
            if(individuo2.infectado() && individuo1.suceptible()){
                individuo1.estado_tPlusDelta = 'i';
            }
        }
    }//se encarga de ejecutar la exposicion, e infectar al individuo correspondente sólo si es suceptible y las probabilidades lo indican
    public void runInfectadoTime(double delta_t){
        if(infectadoTime + delta_t < infectadoLimitTime) {
            infectadoTime += delta_t;
        }else{
            infectadoTime = -1;
            this.estado_tPlusDelta = 'r';
        }
    }//aumenta el tiempo del infectado
    public void initInfectadoTimer(){
        infectadoTime=0;
    }//da inicio al cronometro del infectado simbolicamente cambiando el -1 por un 0
    public void runRecuperadoTime(double delta_t){
        if(recuperadoTime + delta_t < recuperadoLimitTime){
            recuperadoTime+= delta_t;
        }else{
            recuperadoTime = -1;
            this.estado_tPlusDelta = 's';
        }
    }//aumenta el tiempo del recuperado
    public void initRecuperadoTimer(){
        recuperadoTime=0;
    }//da inicio al cronometro del recuperado simbolicamente cambiando el -1 por un 0
    public void computeNextState(double delta_t) {
        double r=Math.random();

        angle+= Math.random()<0.5 ? -Math.random()*deltaAngle:Math.random()*deltaAngle;
        x_tPlusDelta=x+speed*delta_t*Math.cos(angle);
        y_tPlusDelta = y+speed*delta_t*Math.sin(angle);

        if(infectadoTime>=0){
            runInfectadoTime(delta_t);
        }
        if(recuperadoTime >= 0){
            runRecuperadoTime(delta_t);
        }
        if(x_tPlusDelta < 0){   // rebound logic: pared izquierda
            angle = Math.PI - angle;
            x_tPlusDelta = -x_tPlusDelta;
        }
        else if( x_tPlusDelta > comuna.getWidth()){ // rebound logic: pared derecha
            x_tPlusDelta = 2* comuna.getWidth()-x_tPlusDelta;
            angle = Math.PI - angle;
        }
        if(y_tPlusDelta < 0){   // rebound logic: pared inferior
            y_tPlusDelta = -y_tPlusDelta;
            angle = -angle;
        }
        else if( y_tPlusDelta > comuna.getHeight()){  // rebound logic: pared superior
            y_tPlusDelta = 2* comuna.getHeight()-y_tPlusDelta;
            angle = -angle;
        }
    }//se encarga de predecir la próxima posicion del individuo junto con aumentar el tiempo del infectado en caso ser necesario
    public void updateState(){
        x=x_tPlusDelta;
        y=y_tPlusDelta;
        if((estado_tPlusDelta == 'i') && (infectadoTime ==-1)){
            initInfectadoTimer();
        }
        if((estado_tPlusDelta == 'r') && (recuperadoTime == -1)){
            initRecuperadoTimer();
        }
        estado = estado_tPlusDelta;
    }//actualiza los datos actuales por los del tipo tPlusDelta
    public double getX_tPlusDelta(){return this.x_tPlusDelta;}
    public double getY_tPlusDelta(){return this.y_tPlusDelta;}
    public void vacunarse(){
        this.estado_tPlusDelta='v';
    }
}

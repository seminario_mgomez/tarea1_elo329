import java.io.File;
import java.io.IOException;
import java.util.Scanner;

public class Stage5 {
    public static void main(String [] args) throws IOException {
        if (args.length != 1) {
            System.out.println("Usage: java Stage1Main <configurationFile.txt>");
            System.exit(-1);
        }
        Scanner s=new Scanner(new File(args[0]));
        System.out.println("File: "+args[0]);
        double simulationDuration = s.nextDouble();
        int individuos = s.nextInt();
        int infectados = s.nextInt();
        double infectadoLimitTime = s.nextDouble();
        double recuperadoLimitTime = s.nextDouble();
        System.out.println(recuperadoLimitTime);
        System.out.println("Simulation time: "+simulationDuration);
        s.nextLine();
        double comunaWidth = s.nextDouble();
        double comunaLength = s.nextDouble();
        double speed = s.nextDouble();
        double delta_t = s.nextDouble();
        double deltaAngle = s.nextDouble();
        double samplingTime = 1.0;  // 1 [s]
        double distancia = s.nextDouble();
        double mascarillas = s.nextDouble();
        double probabilidad_0 = s.nextDouble();
        double probabilidad_1 = s.nextDouble();
        double probabilidad_2 = s.nextDouble();
        int numVac = s.nextInt();
        double vacSize = s.nextDouble();
        double vacTime = s.nextDouble();
        Comuna comuna = new Comuna(comunaWidth, comunaLength, distancia, probabilidad_0, probabilidad_1 ,probabilidad_2);
        Simulador sim = new Simulador(System.out, comuna, speed, deltaAngle, infectadoLimitTime, individuos, infectados, mascarillas, numVac, vacSize, vacTime, recuperadoLimitTime);
        sim.simulate(delta_t, simulationDuration,samplingTime);
    }
}

JFLAGS = -g
JC = javac
JVM = java 
SALIDA = salida.csv
ENTRADA = Archivo_de_entrada.txt

.SUFFIXES: .java .class 

.java.class:
	$(JC) $(JFLAGS) $*.java

CLASSES = \
	Stage5.java\
	Individuo.java\
	Comuna.java\
	Simulador.java\
	Vacunatorio.java 

MAIN = Stage5

default: classes

classes: $(CLASSES:.java=.class)

run: $(MAIN).class
	$(JVM) $(MAIN) $(ENTRADA) > $(SALIDA)

clean: 
	$(RM) *.class
